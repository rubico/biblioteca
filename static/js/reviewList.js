var showReviewDetails = function(reference){
    var title = $('#review-title-'+reference).html();
    var reviewer = $('#review-reviewer-'+reference).html();
    var reviewerIP = $('#review-reviewer-ip-'+reference).html();
    var createdAt = $('#review-created-at-'+reference).html();
    var rating = $('#review-rating-'+reference).html();
    var summary = $('#review-summary-'+reference).html();

    $('#title-input').val(title);
    $('#reviewer-input').val(reviewer);
    $('#reviewer-ip-input').val(reviewerIP);
    $('#created-at-input').val(createdAt);
    $('#rating-input').val(rating);
    $('#summary-input').val(summary);
};