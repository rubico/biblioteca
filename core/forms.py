from django import forms

from core.models import BookRent
from core.use_cases import available_books


class BookRentForm(forms.ModelForm):
    books = forms.ModelMultipleChoiceField(queryset=available_books())

    class Meta:
        model = BookRent
        fields = ('books', 'members')
