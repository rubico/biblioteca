from datetime import datetime

from django.db.models import Q

from core.models import Book


def apply_rent_date(book_rent):
    book_rent.rent_date = datetime.now()
    return book_rent


def available_books():
    return Book.objects.filter(Q(rents=None))
