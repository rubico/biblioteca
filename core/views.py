from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView

from core.forms import BookRentForm
from core.models import Book, Member, BookRent
from core.use_cases import apply_rent_date


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'


class BookCreateView(LoginRequiredMixin, CreateView):
    template_name = 'book/create.html'
    success_url = reverse_lazy('book_list')
    model = Book
    fields = ['name']


class BookListView(LoginRequiredMixin, ListView):
    template_name = 'book/list.html'
    model = Book


class BookRentView(LoginRequiredMixin, CreateView):
    template_name = 'book/rent.html'
    success_url = reverse_lazy('book_list')
    form_class = BookRentForm

    @transaction.atomic
    def form_valid(self, form):
        book_rent = form.save(commit=False)
        book_rent = apply_rent_date(book_rent)
        book_rent.save()
        form.save_m2m()
        self.object = book_rent
        return HttpResponseRedirect(self.get_success_url())


class MemberCreateView(LoginRequiredMixin, CreateView):
    template_name = 'member/create.html'
    success_url = reverse_lazy('member_list')
    model = Member
    fields = ['name']


class MemberListView(LoginRequiredMixin, ListView):
    template_name = 'member/list.html'
    model = Member
