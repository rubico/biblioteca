from django.conf.urls import url
from django.contrib.auth import views as auth_views

from core.views import BookCreateView, BookListView, MemberCreateView, MemberListView, \
    BookRentView

auth_urls = [
    url(r'^sign_in/$', auth_views.login,
        {'template_name': 'auth/sign_in.html',
         'redirect_authenticated_user': True},
        name='auth_sign_in'),
    url(r'^sign_out/$', auth_views.logout,
        {'next_page': '/auth/sign_in/?next=/'}, name='auth_sign_out'),
]


book_urls = [
    url(r'^$', BookListView.as_view(), name='book_list'),
    url(r'^rent/$', BookRentView.as_view(), name='book_rent'),
    url(r'^create/$', BookCreateView.as_view(), name='book_create')
]


member_urls = [
    url(r'^$', MemberListView.as_view(), name='member_list'),
    url(r'^create/$', MemberCreateView.as_view(), name='member_create')
]