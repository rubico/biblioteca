from datetime import datetime
from django.test import TestCase

from core.factories import BookRentFactory, BookFactory, MemberFactory
from core.use_cases import apply_rent_date, available_books


class ApplyRentDateTestCase(TestCase):
    def test_should_apply_rent_date(self):
        book_rent = BookRentFactory.build()
        self.assertEquals(book_rent.rent_date, None)
        apply_rent_date(book_rent)
        self.assertNotEquals(book_rent.rent_date, None)

    def test_rent_date_should_be_datetime(self):
        book_rent = BookRentFactory.build()
        apply_rent_date(book_rent)
        self.assertEquals(type(book_rent.rent_date), datetime)


class AvailableBooksTestCase(TestCase):
    def test_should_get_not_rented_books(self):
        book = BookFactory()
        av_book = BookFactory()
        member = MemberFactory()
        book_rent = BookRentFactory(rent_date=datetime.now(),
                                    books=(book,), members=(member,))
        av_books = available_books()
        self.assertIn(av_book, av_books)

    def test_should_get_only_not_rented_books(self):
        book = BookFactory()
        BookFactory()
        member = MemberFactory()
        BookRentFactory(rent_date=datetime.now(), books=(book,),
                        members=(member,))
        av_books = available_books()
        self.assertEquals(av_books.count(), 1)