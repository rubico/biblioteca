from unittest.mock import patch, Mock

from django.conf import settings
from django.test import TestCase, RequestFactory
from django.urls import reverse

from core.factories import MemberFactory, BookFactory
from core.views import BookCreateView, MemberCreateView, MemberListView, \
    BookListView, BookRentView, HomeView


class RequestFactoryTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()


class SignInTestCase(TestCase):
    def test_url(self):
        url = reverse('auth_sign_in')
        self.assertEquals('/auth/sign_in/', url)


class SignOutTestCase(TestCase):
    def test_url(self):
        url = reverse('auth_sign_out')
        self.assertEquals('/auth/sign_out/', url)


class HomeTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('home')
        self.assertEquals('/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('home'))
        response = HomeView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)


class BookCreateTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('book_create')
        self.assertEquals('/book/create/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('book_create'))
        response = BookCreateView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)


class BookListTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('book_list')
        self.assertEquals('/book/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('book_list'))
        response = BookListView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)


class MemberCreateTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('member_create')
        self.assertEquals('/member/create/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('member_create'))
        response = MemberCreateView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)


class MemberListTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('member_list')
        self.assertEquals('/member/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('member_list'))
        response = MemberListView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)


class BookRentTestCase(RequestFactoryTestCase):
    def test_url(self):
        url = reverse('book_rent')
        self.assertEquals('/book/rent/', url)

    @patch('django.core.handlers.wsgi.WSGIRequest.user', create=True)
    def test_user_should_be_authenticated(self, mocked_user):
        mocked_user.is_authenticated = False
        request = self.factory.get(reverse('book_rent'))
        response = BookRentView.as_view()(request)
        self.assertIn(settings.LOGIN_URL, response.url)

    @patch('django.forms.models.BaseModelForm._save_m2m', Mock())
    @patch('django.core.handlers.wsgi.WSGIRequest.user', Mock(), create=True)
    @patch('core.views.apply_rent_date')
    def test_form_valid_should_call_apply_rent_time(self, mocked_apply):
        member = MemberFactory()
        book = BookFactory()
        data = {'books': [book.id], 'members': [member.id]}
        request = self.factory.post(reverse('book_rent'), data)
        BookRentView.as_view()(request)
        self.assertTrue(mocked_apply.called)

