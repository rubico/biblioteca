from django.test import TestCase

from core.factories import MemberFactory, BookFactory


class BookTestCase(TestCase):
    def test_str_should_return_name(self):
        book = BookFactory.build()
        name = str(book)
        self.assertEquals(book.name, name)


class MemberTestCase(TestCase):
    def test_str_should_return_name(self):
        member = MemberFactory.build()
        name = str(member)
        self.assertEquals(member.name, name)
