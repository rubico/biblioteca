import factory

from core.models import Member, Book, BookRent


class BookFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Book


class BookRentFactory(factory.django.DjangoModelFactory):
    rent_date = None

    @factory.post_generation
    def books(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for book in extracted:
                self.books.add(book)

    @factory.post_generation
    def members(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for member in extracted:
                self.members.add(member)

    class Meta:
        model = BookRent


class MemberFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Member
